# README #

This is an Open Source spiritual successor to the FetLife for Android application.

The original application was started back in May 2010... long before most of the current frameworks existed.  
Also, for the original version, we had purchased some fonts and images that we do not have license to redistribute.

With that in mind, this will be a fresh/clean rewrite.  
A sterilized snapshot can be found in the [ORIGINAL_ARCHIVE branch](https://bitbucket.org/malachid/fetlife-oss_v2/src/original_archive/).

# Distributions #

## ![Logo](https://bytebucket.org/malachid/fetlife-oss/raw/ORIGINAL_ARCHIVE/src/main/android/res/drawable-mdpi/icon.png) Legacy app ##

The most recent build is 4.2.11.  
This is the one the archive was based on. There are known bugs, namely:

* Adding swipe-to-delete functionality on the feed has impaired the ability to click on links in status updates
* Recent server-side changes have hindered our ability to upload pictures, lock/sticky threads, and possibly other issues.  We haven't patched this.
* A failed experiment turning wall posts into a 'card' layout has made it into the latest build (it was made from the beta and hasn't been reverted)

You can [download it here](https://drive.google.com/file/d/0BxsD8TgBjO5_T0EyMkM3N2FvMVk/view?usp=sharing).

## ![Logo](https://bytebucket.org/malachid/fetlife-oss/raw/default/app/src/main/res/mipmap-mdpi/ic_launcher.png) OSS app ##

There is not yet a functional fetlife-oss build.

# Building #

## Prerequisites ##

* JDK8 as JDK9 broke Gradle by removing rt.jar

## Compiling ##

```
#!bash

./gradlew installDevDebug
```

## Logging ##

To enable full logging from the command line:

```
#!bash

adb shell setprop log.tag.FetLife-OSS VERBOSE
```

# License #

This application is licensed under the [The MIT License](https://bitbucket.org/malachid/fetlife-oss/src/default/LICENSE.md?at=default)


# Attributions #

* The original codebase from GoldenSpiral Software, LLC
* [Android Asset Studio](https://romannurik.github.io/AndroidAssetStudio/index.html) for converting some assets



Copyright (c) 2015 [FetLife-OSS Contributors](https://bitbucket.org/malachid/fetlife-oss/src/default/Contributors.md?at=default)