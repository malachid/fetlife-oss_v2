/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 <a href="https://bitbucket.org/malachid/fetlife-oss/src/default/Contributors.md?at=default">FetLife-OSS Contributors</a>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package net.goldenspiral.fetlifeoss.util;

import java.io.IOException;

/**
 * Created by malachi on 8/8/15.
 */
public class FetLifeException
extends IOException
{
    /**
     * Constructs a new {@code IOException} with its stack trace filled in.
     */
    public FetLifeException() {
        super();
    }

    /**
     * Constructs a new {@code IOException} with its stack trace and detail
     * message filled in.
     *
     * @param msgFmt String format for the detailed message
     * @param args optional arguments for the string format
     */
    public FetLifeException(String msgFmt, Object ... args) {
        super(String.format(msgFmt, args));
    }

    /**
     * Constructs a new {@code IOException} with its stack trace and detail
     * message filled in.
     *
     * @param cause The detail cause for the exception.
     * @param msgFmt String format for the detailed message
     * @param args optional arguments for the string format
     */
    public FetLifeException(Throwable cause, String msgFmt, Object ... args) {
        super(String.format(msgFmt, args), cause);
    }

    /**
     * Constructs a new instance of this class with its detail cause filled in.
     *
     * @param cause The detail cause for the exception.
     * @since 1.6
     */
    public FetLifeException(Throwable cause) {
        super(cause);
    }
}
