/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2015 <a href="https://bitbucket.org/malachid/fetlife-oss/src/default/Contributors.md?at=default">FetLife-OSS Contributors</a>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package net.goldenspiral.fetlifeoss.util;

import android.content.Context;

import net.goldenspiral.fetlifeoss.BuildConfig;
import net.goldenspiral.fetlifeoss.FetLife;
import net.goldenspiral.fetlifeoss.R;

/**
 * Wrap Android's logging facility with some convenience functions
 *
 * To enable full logging from the command line:
 * <code>adb shell setprop log.tag.FetLife-OSS VERBOSE</code>
 *
 * Created by malachi on 7/24/15.
 */
public class Log
{
    private static final String TAG = Log.class.getSimpleName();

    // Version BuildType Tag Thread Message
    private static final String FMT = "%3s%c %-17.15s [%s] %s";

    /**
     * Determine whether logging is enabled
     *
     * @param priority The priority/type of this log message
     * @return True if logging is enabled
     */
    private static boolean isLoggable(final int priority) {
        return android.util.Log.isLoggable(FetLife.TAG, priority);
    }

    public static void initialize(Context context){
        final String appName = context.getResources().getString(R.string.app_name);
        final String hgVersion = context.getResources().getString(R.string.hg_version);
        i(TAG, "Initialized %s %s-%s", appName, BuildConfig.VERSION_NAME, hgVersion);
    }

    /**
     * Low-level logging call
     *
     * @param priority The priority/type of this log message
     * @param tag Used to identify the source of a log message. It usually identifies the class where the log call occurs.
     * @param msg The message you would like logged.
     * @return The number of bytes written.
     */
    private static int println(final int priority, final String tag, final String msg) {
        if(!isLoggable(priority)) return 0;

        return android.util.Log.println(
            priority,
            FetLife.TAG,
            String.format(
                FMT,
                BuildConfig.VERSION_NAME,
                BuildConfig.BUILD_TYPE.charAt(0),
                tag,
                Thread.currentThread().getName(),
                msg
            )
        );
    }

    /**
     * Low-level logging call
     *
     * @param priority The priority/type of this log message
     * @param tag Used to identify the source of a log message. It usually identifies the class where the log call occurs.
     * @param throwable An exception to log
     * @param fmt the format string
     * @param args the list of arguments passed to the formatter. If there are more arguments than required by format, additional arguments are ignored.
     * @return The number of bytes written.
     */
    public static int println(final int priority, final String tag, final Throwable throwable, String fmt, Object ... args) {
        if(!isLoggable(priority)) return 0;

        final String t = (throwable == null ? "" : android.util.Log.getStackTraceString(throwable));
        final String msg = fmt == null ? "" : (args.length > 0 ? String.format(fmt, args) : fmt);

        if (throwable == null) {
            return println(priority, tag, msg);
        } else if (fmt == null) {
            return println(priority, tag, t);
        } else {
            return println(priority, tag, msg + '\n' + t);
        }
    }

    /**
     * Send a VERBOSE log message
     *
     * @param tag Used to identify the source of a log message. It usually identifies the class where the log call occurs.
     * @param throwable An exception to log
     * @return The number of bytes written.
     */
    public static int v(final String tag, final Throwable throwable) {
        return println(android.util.Log.VERBOSE, tag, throwable, null);
    }

    /**
     * Send a VERBOSE log message
     *
     * @param tag Used to identify the source of a log message. It usually identifies the class where the log call occurs.
     * @param fmt the format string
     * @param args the list of arguments passed to the formatter. If there are more arguments than required by format, additional arguments are ignored.
     * @return The number of bytes written.
     */
    public static int v(final String tag, String fmt, Object ... args) {
        return println(android.util.Log.VERBOSE, tag, null, fmt, args);
    }

    /**
     * Send a DEBUG log message
     *
     * @param tag Used to identify the source of a log message. It usually identifies the class where the log call occurs.
     * @param throwable An exception to log
     * @return The number of bytes written.
     */
    public static int d(final String tag, final Throwable throwable) {
        return println(android.util.Log.DEBUG, tag, throwable, null);
    }

    /**
     * Send a DEBUG log message
     *
     * @param tag Used to identify the source of a log message. It usually identifies the class where the log call occurs.
     * @param fmt the format string
     * @param args the list of arguments passed to the formatter. If there are more arguments than required by format, additional arguments are ignored.
     * @return The number of bytes written.
     */
    public static int d(final String tag, String fmt, Object ... args) {
        return println(android.util.Log.DEBUG, tag, null, fmt, args);
    }

    /**
     * Send a INFO log message
     *
     * @param tag Used to identify the source of a log message. It usually identifies the class where the log call occurs.
     * @param throwable An exception to log
     * @return The number of bytes written.
     */
    public static int i(final String tag, final Throwable throwable) {
        return println(android.util.Log.INFO, tag, throwable, null);
    }

    /**
     * Send a INFO log message
     *
     * @param tag Used to identify the source of a log message. It usually identifies the class where the log call occurs.
     * @param fmt the format string
     * @param args the list of arguments passed to the formatter. If there are more arguments than required by format, additional arguments are ignored.
     * @return The number of bytes written.
     */
    public static int i(final String tag, String fmt, Object ... args) {
        return println(android.util.Log.INFO, tag, null, fmt, args);
    }

    /**
     * Send a WARN log message
     *
     * @param tag Used to identify the source of a log message. It usually identifies the class where the log call occurs.
     * @param throwable An exception to log
     * @return The number of bytes written.
     */
    public static int w(final String tag, final Throwable throwable) {
        return println(android.util.Log.WARN, tag, throwable, null);
    }

    /**
     * Send a WARN log message
     *
     * @param tag Used to identify the source of a log message. It usually identifies the class where the log call occurs.
     * @param throwable An exception to log
     * @param fmt the format string
     * @param args the list of arguments passed to the formatter. If there are more arguments than required by format, additional arguments are ignored.
     * @return The number of bytes written.
     */
    public static int w(final String tag, final Throwable throwable, String fmt, Object ... args) {
        return println(android.util.Log.WARN, tag, throwable, fmt, args);
    }

    /**
     * Send a WARN log message
     *
     * @param tag Used to identify the source of a log message. It usually identifies the class where the log call occurs.
     * @param fmt the format string
     * @param args the list of arguments passed to the formatter. If there are more arguments than required by format, additional arguments are ignored.
     * @return The number of bytes written.
     */
    public static int w(final String tag, String fmt, Object ... args) {
        return println(android.util.Log.WARN, tag, null, fmt, args);
    }

    /**
     * Send a ERROR log message
     *
     * @param tag Used to identify the source of a log message. It usually identifies the class where the log call occurs.
     * @param throwable An exception to log
     * @return The number of bytes written.
     */
    public static int e(final String tag, final Throwable throwable) {
        return println(android.util.Log.ERROR, tag, throwable, null);
    }

    /**
     * Send a ERROR log message
     *
     * @param tag Used to identify the source of a log message. It usually identifies the class where the log call occurs.
     * @param throwable An exception to log
     * @param fmt the format string
     * @param args the list of arguments passed to the formatter. If there are more arguments than required by format, additional arguments are ignored.
     * @return The number of bytes written.
     */
    public static int e(final String tag, final Throwable throwable, String fmt, Object ... args) {
        return println(android.util.Log.ERROR, tag, throwable, fmt, args);
    }

    /**
     * Send a ERROR log message
     *
     * @param tag Used to identify the source of a log message. It usually identifies the class where the log call occurs.
     * @param fmt the format string
     * @param args the list of arguments passed to the formatter. If there are more arguments than required by format, additional arguments are ignored.
     * @return The number of bytes written.
     */
    public static int e(final String tag, String fmt, Object ... args) {
        return println(android.util.Log.ERROR, tag, null, fmt, args);
    }
}
