/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 <a href="https://bitbucket.org/malachid/fetlife-oss/src/default/Contributors.md?at=default">FetLife-OSS Contributors</a>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package net.goldenspiral.fetlifeoss.rest.interceptors;

import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import net.goldenspiral.fetlifeoss.util.Log;

import java.io.IOException;

/**
 * Created by malachi on 10/8/15.
 * from https://github.com/square/okhttp/wiki/Interceptors
 */
public class LoggingInterceptor
implements Interceptor
{
    private static final String TAG = LoggingInterceptor.class.getSimpleName();
    private boolean enabled;

    public LoggingInterceptor(boolean enabled)
    {
        this.enabled = enabled;
    }

    @Override
    public Response intercept(Chain chain) throws IOException
    {
        Request request = chain.request();

        long t1 = System.nanoTime();
        if(enabled)
        {
            Log.i(TAG, String.format(
                "Sending request %s on %s%n%s",
                request.url(),
                chain.connection(),
                request.headers()
            ));
        }

        Response response = chain.proceed(request);

        long t2 = System.nanoTime();
        if(enabled)
        {
            Log.i(TAG, String.format(
                "Received response for %s in %.1fms%n%s",
                response.request().url(),
                (t2 - t1) / 1e6d,
                response.headers()
            ));
        }

        return response;
    }
}
