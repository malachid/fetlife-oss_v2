/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 <a href="https://bitbucket.org/malachid/fetlife-oss/src/default/Contributors.md?at=default">FetLife-OSS Contributors</a>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package net.goldenspiral.fetlifeoss.rest.model;

import android.view.View;

import com.google.gson.annotations.SerializedName;

import net.goldenspiral.fetlifeoss.FetLife;
import net.goldenspiral.fetlifeoss.bus.GenericClickEvent;
import net.goldenspiral.fetlifeoss.bus.NavItemClicked;

/**
 * Created by malachi on 8/8/15.
 */
public class User
{
    private long id;
    private String nickname;
    private int age;
    private String gender;
    private String location;
    private String role;

    @SerializedName("supporter_badge")
    private boolean supporter;

    @SerializedName("medium_avatar_url")
    private String mediumAvatarUrl;

    @SerializedName("mini_avatar_url")
    private String miniAvatarUrl;

    @SerializedName("small_avatar_url")
    private String smallAvatarUrl;

    @SerializedName("new_conversation_url")
    private String newConversationUrl;

    @SerializedName("new_wall_post_url")
    private String newWallPostUrl;

    @SerializedName("pictures_url")
    private String picturesUrl;

    @SerializedName("posts_url")
    private String postsUrl;

    @SerializedName("profile_url")
    private String profileUrl;

    @SerializedName("videos_url")
    private String videosUrl;

    public long getId() {
        return id;
    }

    public int getAge() {
        return age;
    }

    public String getGender() {
        return gender;
    }

    public String getLocation() {
        return location;
    }

    public String getMiniAvatarUrl() {
        return miniAvatarUrl;
    }

    public String getMediumAvatarUrl() {
        return mediumAvatarUrl;
    }

    public String getNewConversationUrl() {
        return newConversationUrl;
    }

    public String getNewWallPostUrl() {
        return newWallPostUrl;
    }

    public String getNickname() {
        return nickname;
    }

    public String getPicturesUrl() {
        return picturesUrl;
    }

    public String getPostsUrl() {
        return postsUrl;
    }

    public String getProfileUrl() {
        return profileUrl;
    }

    public String getRole() {
        return role;
    }

    public String getSmallAvatarUrl() {
        return smallAvatarUrl;
    }

    public boolean isSupporter() {
        return supporter;
    }

    public String getVideosUrl() {
        return videosUrl;
    }

    public void onClick(View view)
    {
        FetLife.getBus().post(new GenericClickEvent(getProfileUrl()));
    }
}
