/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 <a href="https://bitbucket.org/malachid/fetlife-oss/src/default/Contributors.md?at=default">FetLife-OSS Contributors</a>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package net.goldenspiral.fetlifeoss.rest.service;

import com.google.gson.Gson;
import com.squareup.okhttp.OkHttpClient;

import net.goldenspiral.fetlifeoss.BuildConfig;
import net.goldenspiral.fetlifeoss.rest.converters.ToStringConverterFactory;
import net.goldenspiral.fetlifeoss.rest.interceptors.ContentTypeInterceptor;
import net.goldenspiral.fetlifeoss.rest.interceptors.LoggingInterceptor;
import net.goldenspiral.fetlifeoss.rest.model.Counts;
import net.goldenspiral.fetlifeoss.rest.model.FriendsPage;
import net.goldenspiral.fetlifeoss.rest.model.Like;
import net.goldenspiral.fetlifeoss.rest.model.NumPages;
import net.goldenspiral.fetlifeoss.rest.model.PictureRef;
import net.goldenspiral.fetlifeoss.rest.model.Session;
import net.goldenspiral.fetlifeoss.rest.model.User;
import net.goldenspiral.fetlifeoss.rest.model.UserGroups;
import net.goldenspiral.fetlifeoss.rest.model.VideoRef;
import net.goldenspiral.fetlifeoss.util.gson.GsonUtil;

import java.util.List;

import retrofit.GsonConverterFactory;
import retrofit.Retrofit;
import retrofit.RxJavaCallAdapterFactory;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;
import rx.Observable;

/**
 * Created by malachi on 8/8/15.
 */
public class FetLifeService
{
    // @TODO flavor this if we ever get a dev url
    public static final String ENDPOINT = "https://fetlife.com";

    private FetLifeAPI api;

    public FetLifeService()
    {
        final OkHttpClient client = new OkHttpClient();
        client.interceptors().add(new ContentTypeInterceptor());
        client.networkInterceptors().add(new LoggingInterceptor(BuildConfig.DEBUG));

        Gson gson = GsonUtil.instance
            .getBuilder().create();

        Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(ENDPOINT)
            .client(client)
            .addConverterFactory(new ToStringConverterFactory())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
            .build();

        api = retrofit.create(FetLifeAPI.class);
    }

    public FetLifeAPI getAPI(){return api;}

    public interface FetLifeAPI
    {
        /**
         * Login to the server and establish a session.
         *
         * @param user username or email
         * @param password password
         * @param remember true, as a string
         * @param emptyBody reference https://github.com/square/retrofit/issues/854
         * @return a Session observable
         */
        @POST("/session.json")
        Observable<Session> login(@Query("nickname_or_email") String user, @Query("password") String password, @Query("remember_me") String remember, @Body String emptyBody);

        /**
         * Grab information about me.
         * Requires calling login successfully first.
         *
         * @return a User observable.
         */
        @GET("/me.json")
        Observable<User> me();

        @GET("/users/{userid}.json")
        Observable<User> user(@Path("userid") long userId);

        /**
         * Poll the count information for new messages, friendship requests and ats.
         *
         * @return Counts
         */
        @GET("/polling/counts.json?fetch=new_messages,friendship_requests,ats")
        Observable<Counts> counts();

        /**
         * Get the list of stuff that I love
         *
         * @param myUserId as it only works for me
         * @return list of Like
         */
        @GET("/users/{userid}/likes_v4.json")
        Observable<List<Like>> stuffILove(@Path("userid") long myUserId);

        /**
         * Get a list of my groups
         *
         * @return UserGroups
         */
        @GET("/groups.json")
        Observable<UserGroups> myGroups();

        /**
         * Number of pages of pictures
         *
         * @param userId of the user
         * @return NumPages
         */
        @GET("/users/{userid}/pictures/number_of_pages.json")
        Observable<NumPages> numPicturePages(@Path("userid") long userId);

        /**
         * Pictures for a specific user
         *
         * @param userId of the user
         * @param pageNum between 1 and numPicturePages
         * @return list of PictureRef
         */
        @GET("/users/{userid}/pictures.json")
        Observable<List<PictureRef>> pictures(@Path("userid") long userId, @Query("page") int pageNum);

        /**
         * Number of pages of videos
         *
         * @param userId of the user
         * @return NumPages
         */
        @GET("/users/{userid}/videos/number_of_pages.json")
        Observable<NumPages> numVideoPages(@Path("userid") long userId);

        /**
         * Videos for a specific user
         *
         * @param userId of the user
         * @param pageNum between 1 and numVideoPages
         * @return list of VideoRef
         */
        @GET("/users/{userid}/videos.json")
        Observable<List<VideoRef>> videos(@Path("userid") long userId, @Query("page") int pageNum);

        /**
         * @TODO 500 server error on server side. Disabled.
         * Number of pages of friends
         *
         * @param userId of the user
         * @return NumPages
         */
//        @GET("/users/{userid}/friends/number_of_pages.json")
//        Observable<NumPages> numFriendPages(@Path("userid") long userId);

        /**
         * Friends for a specific user
         *
         * @param userId of the user
         * @param pageNum between 1 and numFriendPages
         * @return list of FriendsPage
         */
        @GET("/users/{userid}/friends.json")
        Observable<FriendsPage> friends(@Path("userid") long userId, @Query("page") int pageNum);

        /**
         * @TODO returns /home Disabled.
         * Number of pages of mutual friends
         *
         * @param userId of the user
         * @return NumPages
         */
//        @GET("/users/{userid}/friends/mutual/number_of_pages.json")
//        Observable<NumPages> numMutualFriendPages(@Path("userid") long userId);

        /**
         * Friends for a specific user
         *
         * @param userId of the user
         * @param pageNum between 1 and numMutualFriendPages
         * @return list of FriendsPage
         */
        @GET("/users/{userid}/friends/mutual.json")
        Observable<FriendsPage> mutualFriends(@Path("userid") long userId, @Query("page") int pageNum);
    }

}
