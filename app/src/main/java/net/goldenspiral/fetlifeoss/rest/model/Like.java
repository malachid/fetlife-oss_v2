/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 <a href="https://bitbucket.org/malachid/fetlife-oss/src/default/Contributors.md?at=default">FetLife-OSS Contributors</a>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package net.goldenspiral.fetlifeoss.rest.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by malachi on 8/12/15.
 */
public class Like
{
    @SerializedName("comments_count")
    private long commentCount;

    // @TODO maybe use gsonfirebuilder to store these as long instead so we can format per user device preference
    private String date;

    private String id;

    @SerializedName("likes_count")
    private long likeCount;

    @SerializedName("picture_link_url")
    private String linkUrl;

    @SerializedName("picture_url")
    private String imgUrl;

    // @TODO enum?
    private String type;

    @SerializedName("user_age")
    private int age;

    @SerializedName("user_avatar_url")
    private String avatarUrl;

    @SerializedName("user_location")
    private String location;

    @SerializedName("user_nickname")
    private String nickname;

    @SerializedName("user_profile_link_url")
    private String profileUrl;

    @SerializedName("user_role_short_form")
    private String role;

    @SerializedName("user_sex")
    private String gender;

    public long getCommentCount() {
        return commentCount;
    }

    public String getDate() {
        return date;
    }

    public String getId() {
        return id;
    }

    public long getLikeCount() {
        return likeCount;
    }

    public String getLinkUrl() {
        return linkUrl;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public String getType() {
        return type;
    }

    public int getAge() {
        return age;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public String getLocation() {
        return location;
    }

    public String getNickname() {
        return nickname;
    }

    public String getProfileUrl() {
        return profileUrl;
    }

    public String getRole() {
        return role;
    }

    public String getGender() {
        return gender;
    }
}
