/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2015 <a href="https://bitbucket.org/malachid/fetlife-oss/src/default/Contributors.md?at=default">FetLife-OSS Contributors</a>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package net.goldenspiral.fetlifeoss;

import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.squareup.otto.Bus;

import net.goldenspiral.fetlifeoss.auth.FetLifeAccountManager;
import net.goldenspiral.fetlifeoss.cookie.MultiUserCookieStore;
import net.goldenspiral.fetlifeoss.rest.service.FetLifeService;
import net.goldenspiral.fetlifeoss.util.Log;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;

/**
 * Created by malachi on 7/24/15.
 */
public class FetLife
extends Application
{
    public static final String TAG = "FetLife-OSS";

    private static SharedPreferences prefs;
    public static SharedPreferences getPreferences(){return prefs;}

    private static FetLifeService fetLifeService;
    public static FetLifeService.FetLifeAPI getServer(){return fetLifeService.getAPI();}

    private static FetLifeAccountManager acctManager;
    public static FetLifeAccountManager getAccountManager(){return acctManager;}

    private static Bus bus;
    public static Bus getBus(){return bus;}

    /**
     * Called when the application is starting, before any activity, service,
     * or receiver objects (excluding content providers) have been created.
     * Implementations should be as quick as possible (for example using
     * lazy initialization of state) since the time spent in this function
     * directly impacts the performance of starting the first activity,
     * service, or receiver in a process.
     * If you override this method, be sure to call super.onCreate().
     */
    @Override
    public void onCreate() {
        super.onCreate();
        Log.initialize(this);
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        bus = new Bus();
        acctManager = new FetLifeAccountManager(this);
        CookieHandler.setDefault(new CookieManager(new MultiUserCookieStore(this), CookiePolicy.ACCEPT_ALL));
        fetLifeService = new FetLifeService();
    }
}
