/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 <a href="https://bitbucket.org/malachid/fetlife-oss/src/default/Contributors.md?at=default">FetLife-OSS Contributors</a>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package net.goldenspiral.fetlifeoss.sync;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;

import net.goldenspiral.fetlifeoss.FetLife;
import net.goldenspiral.fetlifeoss.bus.CountsReceivedEvent;
import net.goldenspiral.fetlifeoss.rest.model.Counts;
import net.goldenspiral.fetlifeoss.util.Log;

import java.util.Calendar;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by malachi on 10/11/15.
 */
public class PeriodicSync
extends BroadcastReceiver
{
    private static final String TAG = PeriodicSync.class.getSimpleName();
    public static final String ARG_ENABLED = PeriodicSync.class.getName() + ":enabled";
    private static final String ARG_PERIODIC = PeriodicSync.class.getName() + ":periodic";
    private static int REQUEST_CODE = -1;
    private static PendingIntent pendingIntent = null;
    private static long syncIntervalInMS = 1000 * 60 * 60;
    private CountsReceivedEvent lastEvent;

    public static Intent createIntent(Context context, boolean syncEnabled)
    {
        Intent intent = new Intent(context, PeriodicSync.class);
        intent.putExtra(ARG_ENABLED, true);
        return intent;
    }

    /**
     * This method is called when the BroadcastReceiver is receiving an Intent
     * broadcast.  During this time you can use the other methods on
     * BroadcastReceiver to view/modify the current result values.  This method
     * is always called within the main thread of its process, unless you
     * explicitly asked for it to be scheduled on a different thread using
     * {@link Context#registerReceiver(BroadcastReceiver,
     * IntentFilter, String, Handler)}. When it runs on the main
     * thread you should
     * never perform long-running operations in it (there is a timeout of
     * 10 seconds that the system allows before considering the receiver to
     * be blocked and a candidate to be killed). You cannot launch a popup dialog
     * in your implementation of onReceive().
     * <p/>
     * <p><b>If this BroadcastReceiver was launched through a &lt;receiver&gt; tag,
     * then the object is no longer alive after returning from this
     * function.</b>  This means you should not perform any operations that
     * return a result to you asynchronously -- in particular, for interacting
     * with services, you should use
     * {@link Context#startService(Intent)} instead of
     * {@link Context#bindService(Intent, ServiceConnection, int)}.  If you wish
     * to interact with a service that is already running, you can use
     * {@link #peekService}.
     * <p/>
     * <p>The Intent filters used in {@link Context#registerReceiver}
     * and in application manifests are <em>not</em> guaranteed to be exclusive. They
     * are hints to the operating system about how to find suitable recipients. It is
     * possible for senders to force delivery to specific recipients, bypassing filter
     * resolution.  For this reason, {@link #onReceive(Context, Intent) onReceive()}
     * implementations should respond only to known actions, ignoring any unexpected
     * Intents that they may receive.
     *
     * @param context The Context in which the receiver is running.
     * @param intent  The Intent being received.
     */
    @Override
    public void onReceive(Context context, Intent intent)
    {
        // @TODO if(FetLife.isSyncEnabled())
        if(hasFlag(intent, ARG_ENABLED)) {
            // Sent by applications settings to change enabled state
            setAlarmEnabled(context, intent.getBooleanExtra(ARG_ENABLED, false));
        }else if(hasFlag(intent, ARG_PERIODIC)){
            sync(context);
        }else{
            // Sent by boot completion
            setAlarmEnabled(context, true);
        }
    }

    private boolean hasFlag(Intent intent, String flag)
    {
        if(intent == null) return false;
        return (intent.hasExtra(flag) && intent.getBooleanExtra(flag, false));
    }

    private void sync(final Context context)
    {
        // @TODO FetLifeAccountManager.validate() on 302
        FetLife.getServer().counts()
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Subscriber<Counts>() {
                @Override
                public void onCompleted() {
                    setAlarmEnabled(context, true);
                }

                @Override
                public void onError(Throwable e) {
                    // @TODO better error handling... maybe via notification?
                    Log.d(TAG, "Unable to sync counts with server. Disabling alarm.: %s", e.getMessage() );
                    setAlarmEnabled(context, false);
                }

                @Override
                public void onNext(Counts counts) {
                    lastEvent = new CountsReceivedEvent(counts);
                    FetLife.getBus().post(lastEvent);
                }
            });
    }

    public void setAlarmEnabled(Context context, boolean enable) {
        final AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        if(!enable)
        {
            if(pendingIntent != null)
                am.cancel(pendingIntent);

            return;
        }

        final Calendar cal = Calendar.getInstance();
        // @TODO make this a setting
        cal.add(Calendar.MINUTE, 1);
        Intent intent = new Intent(context, getClass());
        intent.putExtra(ARG_PERIODIC, true);
        // @TODO any other setup?
        if(REQUEST_CODE == -1) REQUEST_CODE = hashCode();
        pendingIntent = PendingIntent.getBroadcast(context, REQUEST_CODE, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        // @TODO move intervalInMS to settings
        am.setInexactRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), syncIntervalInMS, pendingIntent);
    }
}
