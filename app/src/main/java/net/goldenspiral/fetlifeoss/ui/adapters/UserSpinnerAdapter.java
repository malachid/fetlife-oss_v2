/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 <a href="https://bitbucket.org/malachid/fetlife-oss/src/default/Contributors.md?at=default">FetLife-OSS Contributors</a>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package net.goldenspiral.fetlifeoss.ui.adapters;

import android.accounts.Account;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SpinnerAdapter;

import net.goldenspiral.fetlifeoss.FetLife;
import net.goldenspiral.fetlifeoss.auth.FetLifeAccountManager;
import net.goldenspiral.fetlifeoss.auth.NoAccountsException;
import net.goldenspiral.fetlifeoss.databinding.ListUserTypeBinding;
import net.goldenspiral.fetlifeoss.rest.model.User;
import net.goldenspiral.fetlifeoss.ui.fragments.FetLifeFragment;

/**
 * Created by malachi on 8/10/15.
 */
public class UserSpinnerAdapter
extends BaseAdapter
implements SpinnerAdapter
{
    private FetLifeFragment fragment;
    private FetLifeAccountManager acctMgr;

    public UserSpinnerAdapter(final FetLifeFragment fragment)
    {
        super();
        this.fragment = fragment;
        acctMgr = FetLife.getAccountManager();
    }

    protected Context getContext()
    {
        return fragment.getActivity();
    }

    /**
     * How many items are in the data set represented by this Adapter.
     *
     * @return Count of items.
     */
    @Override
    public int getCount() {
        try {
            return acctMgr.getAccounts().length;
        } catch (NoAccountsException e) {
            return 0;
        }
    }

    /**
     * Get the data item associated with the specified position in the data set.
     *
     * @param position Position of the item whose data we want within the adapter's
     *                 data set.
     * @return The data at the specified position.
     */
    @Override
    public Object getItem(int position) {
        return getAccount(position);
    }

    public Account getAccount(int position) {
        try {
            return acctMgr.getAccounts()[position];
        } catch (NoAccountsException e) {
            return null;
        }
    }

    public User getUser(int position) {
        Account acct = getAccount(position);
        if(acct == null) return null;
        return acctMgr.getUser(acct);
    }

    /**
     * Get the row id associated with the specified position in the list.
     *
     * @param position The position of the item within the adapter's data set whose row id we want.
     * @return The id of the item at the specified position.
     */
    @Override
    public long getItemId(int position) {
        return position;
    }

    /**
     * Get a View that displays the data at the specified position in the data set. You can either
     * create a View manually or inflate it from an XML layout file. When the View is inflated, the
     * parent View (GridView, ListView...) will apply default layout parameters unless you use
     * {@link LayoutInflater#inflate(int, ViewGroup, boolean)}
     * to specify a root view and to prevent attachment to the root.
     *
     * @param position    The position of the item within the adapter's data set of the item whose view
     *                    we want.
     * @param convertView The old view to reuse, if possible. Note: You should check that this view
     *                    is non-null and of an appropriate type before using. If it is not possible to convert
     *                    this view to display the correct data, this method can create a new view.
     *                    Heterogeneous lists can specify their number of view types, so that this View is
     *                    always of the right type (see {@link #getViewTypeCount()} and
     *                    {@link #getItemViewType(int)}).
     * @param parent      The parent that this view will eventually be attached to
     * @return A View corresponding to the data at the specified position.
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ListUserTypeBinding holder = null;

        if(convertView == null)
        {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            holder = ListUserTypeBinding.inflate(inflater, parent, false);
            convertView = holder.getRoot();
            convertView.setTag(holder);
        }else{
            holder = (ListUserTypeBinding) convertView.getTag();
        }

        // @TODO handle null User
        holder.setData(getUser(position));
        // @TODO add functionality for holder.ffa (used to be 'android' but that won't compile now)

        return convertView;
    }



}
