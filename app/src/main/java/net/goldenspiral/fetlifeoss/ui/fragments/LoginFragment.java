/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 <a href="https://bitbucket.org/malachid/fetlife-oss/src/default/Contributors.md?at=default">FetLife-OSS Contributors</a>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package net.goldenspiral.fetlifeoss.ui.fragments;

import android.accounts.Account;
import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.TextView;

import net.goldenspiral.fetlifeoss.BuildConfig;
import net.goldenspiral.fetlifeoss.FetLife;
import net.goldenspiral.fetlifeoss.R;
import net.goldenspiral.fetlifeoss.auth.NoAccountsException;
import net.goldenspiral.fetlifeoss.rest.model.Session;
import net.goldenspiral.fetlifeoss.rest.model.User;
import net.goldenspiral.fetlifeoss.util.Log;

import java.util.ArrayList;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by malachi on 8/8/15.
 */
public class LoginFragment
extends FetLifeFragment
{
    private static final String TAG = LoginFragment.class.getSimpleName();
    private AutoCompleteTextView emailField;
    private EditText pwdField;
    private TextInputLayout pwdLayout;

    public static LoginFragment newInstance()
    {
        return new LoginFragment();
    }

    /**
     * Default constructor.  <strong>Every</strong> fragment must have an
     * empty constructor, so it can be instantiated when restoring its
     * activity's state.  It is strongly recommended that subclasses do not
     * have other constructors with parameters, since these constructors
     * will not be called when the fragment is re-instantiated; instead,
     * arguments can be supplied by the caller with {@link #setArguments}
     * and later retrieved by the Fragment with {@link #getArguments}.
     * <p/>
     * <p>Applications should generally not implement a constructor.  The
     * first place application code an run where the fragment is ready to
     * be used is in {@link Fragment#onAttach(Activity)}, the point where the fragment
     * is actually associated with its activity.  Some applications may also
     * want to implement {@link #onInflate} to retrieve attributes from a
     * layout resource, though should take care here because this happens for
     * the fragment is attached to its activity.
     */
    public LoginFragment() {
        super();
    }

    /**
     * Title resource id.
     *
     * @return title resource identifier
     */
    @Override
    public int getTitle() {
        return R.string.login_title;
    }

    /**
     * Called to have the fragment instantiate its user interface view.
     * This is optional, and non-graphical fragments can return null (which
     * is the default implementation).  This will be called between
     * {@link #onCreate(Bundle)} and {@link #onActivityCreated(Bundle)}.
     * <p/>
     * <p>If you return a View from here, you will later be called in
     * {@link #onDestroyView} when the view is being released.
     *
     * @param inflater           The LayoutInflater object that can be used to inflate
     *                           any views in the fragment,
     * @param container          If non-null, this is the parent view that the fragment's
     *                           UI should be attached to.  The fragment should not add the view itself,
     *                           but this can be used to generate the LayoutParams of the view.
     * @param savedInstanceState If non-null, this fragment is being re-constructed
     *                           from a previous saved state as given here.
     * @return Return the View for the fragment's UI, or null.
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setRootView(inflate(inflater, container, R.layout.fragment_login));
        emailField = (AutoCompleteTextView) getRootView().findViewById(R.id.login_email);
        pwdField = (EditText) getRootView().findViewById(R.id.login_pwd);
        pwdLayout = (TextInputLayout) getRootView().findViewById(R.id.login_pwd_layout);
        ((TextView) getRootView().findViewById(R.id.login_version)).setText(BuildConfig.VERSION_NAME);
        setOnClickListeners(R.id.login_btn_ok);

        try{
            final ArrayList<String> existingAccountNames = new ArrayList<>();
            for(Account account : FetLife.getAccountManager().getAccounts())
                existingAccountNames.add(account.name);

            emailField.setAdapter(new ArrayAdapter<>(
                    getActivity(), android.R.layout.simple_dropdown_item_1line, existingAccountNames));
        } catch (NoAccountsException e) {
            // no-op?
        }

        return getRootView();
    }

    /**
     * Called when a view has been clicked.
     *
     * @param view The view that was clicked.
     */
    @Override
    public void onClick(View view) {
        switch(view.getId())
        {
            case R.id.login_btn_ok:
                doLogin();
                break;
            default:
                super.onClick(view);
        }
    }

    private void doLogin() {
        final String email = emailField.getText().toString();
        final String pwd = pwdField.getText().toString();
        boolean error = TextUtils.isEmpty(email) || TextUtils.isEmpty(pwd);
        setPasswordError(error ? getString(R.string.login_empty) : null);
        if(error) return;

        // Set a null account before registering so that cookies don't corrupt the current user
        FetLife.getAccountManager().setCurrentAccount(null);

        FetLife.getServer().login(email, pwd, "true", "")
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Subscriber<Session>() {
                @Override
                public void onCompleted() {
                    // no-op
                }

                @Override
                public void onError(Throwable e) {
                    Log.e(TAG, e, "Error logging in as %s", email);
                    setPasswordError(e.getMessage());
                }

                @Override
                public void onNext(Session session) {
                    // @TODO this is temporary
                    Log.d(TAG, "Connected. TOKEN=%s", session.getToken());
                    grabMe(email, pwd);
                }
            });
    }

    private void grabMe(final String email, final String pwd)
    {
        FetLife.getServer().me()
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Subscriber<User>() {
                @Override
                public void onCompleted() {
                    setDrawerLocked(false);
                    // @TODO temporary
                    getMainActivity().swapPage(EmptyFragment.newInstance());
                }

                @Override
                public void onError(Throwable e) {
                    Log.e(TAG, e, "Error getting my information");
                    setPasswordError(e.getMessage());
                }

                @Override
                public void onNext(User user) {
                    Log.d(TAG, "Create an account for %s, user #%d", user.getNickname(), user.getId());
                    FetLife.getAccountManager().createAccount(getActivity(), user, email, pwd);
                }
            });
    }

    private void setPasswordError(final String message)
    {
        boolean hasError = !TextUtils.isEmpty(message);
        pwdLayout.setErrorEnabled(hasError);
        if(hasError)
            pwdLayout.setError(message);
    }
}
