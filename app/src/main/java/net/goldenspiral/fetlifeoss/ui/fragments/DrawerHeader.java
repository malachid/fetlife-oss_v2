/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 <a href="https://bitbucket.org/malachid/fetlife-oss/src/default/Contributors.md?at=default">FetLife-OSS Contributors</a>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package net.goldenspiral.fetlifeoss.ui.fragments;

import android.accounts.Account;
import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;
import com.squareup.otto.Subscribe;
import net.goldenspiral.fetlifeoss.FetLife;
import net.goldenspiral.fetlifeoss.R;
import net.goldenspiral.fetlifeoss.auth.FetLifeAccountManager;
import net.goldenspiral.fetlifeoss.bus.AccountCreatedEvent;
import net.goldenspiral.fetlifeoss.bus.LoginEvent;
import net.goldenspiral.fetlifeoss.ui.adapters.UserSpinnerAdapter;

/**
 * Created by malachi on 8/9/15.
 */
public class DrawerHeader
extends FetLifeFragment
implements AdapterView.OnItemSelectedListener
{
    private static final String TAG = DrawerHeader.class.getSimpleName();
    private UserSpinnerAdapter userAdapter;
    private Spinner userSpinner;
    private FetLifeAccountManager acctMgr;

    /**
     * Default constructor.  <strong>Every</strong> fragment must have an
     * empty constructor, so it can be instantiated when restoring its
     * activity's state.  It is strongly recommended that subclasses do not
     * have other constructors with parameters, since these constructors
     * will not be called when the fragment is re-instantiated; instead,
     * arguments can be supplied by the caller with {@link #setArguments}
     * and later retrieved by the Fragment with {@link #getArguments}.
     * <p/>
     * <p>Applications should generally not implement a constructor.  The
     * first place application code an run where the fragment is ready to
     * be used is in {@link #onAttach(Activity)}, the point where the fragment
     * is actually associated with its activity.  Some applications may also
     * want to implement {@link #onInflate} to retrieve attributes from a
     * layout resource, though should take care here because this happens for
     * the fragment is attached to its activity.
     */
    public DrawerHeader() {
        super();
    }

    /**
     * Called to have the fragment instantiate its user interface view.
     * This is optional, and non-graphical fragments can return null (which
     * is the default implementation).  This will be called between
     * {@link #onCreate(Bundle)} and {@link #onActivityCreated(Bundle)}.
     * <p/>
     * <p>If you return a View from here, you will later be called in
     * {@link #onDestroyView} when the view is being released.
     *
     * @param inflater           The LayoutInflater object that can be used to inflate
     *                           any views in the fragment,
     * @param container          If non-null, this is the parent view that the fragment's
     *                           UI should be attached to.  The fragment should not add the view itself,
     *                           but this can be used to generate the LayoutParams of the view.
     * @param savedInstanceState If non-null, this fragment is being re-constructed
     *                           from a previous saved state as given here.
     * @return Return the View for the fragment's UI, or null.
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setRootView(inflate(inflater, container, R.layout.fragment_drawer_header));
        userSpinner = (Spinner) getRootView().findViewById(R.id.drawer_header_me);
        userAdapter = new UserSpinnerAdapter(this);
        userSpinner.setAdapter(userAdapter);
        selectCurrentAccount();
        userSpinner.setOnItemSelectedListener(this);
        setOnClickListeners(R.id.drawer_header_addaccount);
        return getRootView();
    }

    /**
     * Called when a view has been clicked.
     *
     * @param view The view that was clicked.
     */
    @Override
    public void onClick(View view) {
        switch(view.getId())
        {
            case R.id.drawer_header_addaccount:
                getMainActivity().swapPage(LoginFragment.newInstance());
                break;
            default:
                break;
        }
    }

    private void selectCurrentAccount()
    {
        if(acctMgr.getCurrentAccount() == null) return;
        final String currentName = acctMgr.getCurrentAccount().name;
        for(int i=0; i< userAdapter.getCount(); i++)
        {
            if(!userAdapter.getAccount(i).name.equals(currentName)) continue;
            userSpinner.setSelection(i);
        }
    }

    @Subscribe public void accountCreated(AccountCreatedEvent event)
    {
        if(userAdapter == null) return;
        userAdapter.notifyDataSetChanged();
    }

    @Subscribe public void login(LoginEvent event)
    {
        if(userAdapter == null) return;
        selectCurrentAccount();
    }

    /**
     * <p>Callback method to be invoked when an item in this view has been
     * selected. This callback is invoked only when the newly selected
     * position is different from the previously selected position or if
     * there was no selected item.</p>
     * <p/>
     * Impelmenters can call getItemAtPosition(position) if they need to access the
     * data associated with the selected item.
     *
     * @param parent   The AdapterView where the selection happened
     * @param view     The view within the AdapterView that was clicked
     * @param position The position of the view in the adapter
     * @param id       The row id of the item that is selected
     */
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Account account = userAdapter.getAccount(position);
        if(account == null) return;
        acctMgr.setCurrentAccount(account);
    }

    /**
     * Callback method to be invoked when the selection disappears from this
     * view. The selection can disappear for instance when touch is activated
     * or when the adapter becomes empty.
     *
     * @param parent The AdapterView that now contains no selected item.
     */
    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    /**
     * Called when a fragment is first attached to its activity.
     * {@link #onCreate(Bundle)} will be called after this.
     *
     * @param activity
     */
    @Override
    @SuppressWarnings("deprecated")
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        acctMgr = FetLife.getAccountManager();
        FetLife.getBus().register(this);
    }

    /**
     * Called when the fragment is no longer attached to its activity.  This
     * is called after {@link #onDestroy()}.
     */
    @Override
    public void onDetach() {
        FetLife.getBus().unregister(this);
        super.onDetach();
    }
}
