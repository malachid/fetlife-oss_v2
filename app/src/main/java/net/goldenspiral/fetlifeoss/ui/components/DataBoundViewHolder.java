package net.goldenspiral.fetlifeoss.ui.components;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import net.goldenspiral.fetlifeoss.BR;
import net.goldenspiral.fetlifeoss.R;

/**
 * Created by malachi on 10/10/15.
 */
public class DataBoundViewHolder extends RecyclerView.ViewHolder
{
    public static DataBoundViewHolder create(ViewGroup parent, int viewTypeLayout)
    {
        // @TODO let's cache this and not re-create it every time

        // create ContextThemeWrapper from the original Activity Context with the custom theme
        final Context contextThemeWrapper = new ContextThemeWrapper(parent.getContext(), R.style.AppTheme);

        // clone the inflater using the ContextThemeWrapper
        LayoutInflater localInflater = LayoutInflater.from(parent.getContext()).cloneInContext(contextThemeWrapper);

        // inflate the layout using the cloned inflater, not default inflater
        // @TODO AndroidStudio shows an error for FetLifeDatabinder - see it for details
        return new DataBoundViewHolder(DataBindingUtil.inflate(localInflater, viewTypeLayout, parent, false, FetLifeDatabinder.INSTANCE));
    }

    private ViewDataBinding binding;

    public DataBoundViewHolder(ViewDataBinding binding)
    {
        super(binding.getRoot());
        this.binding = binding;
    }

    public ViewDataBinding getBinding() {
        return binding;
    }

    public void bindTo(Object obj)
    {
        binding.setVariable(BR.data, obj);
        binding.executePendingBindings();
    }
}
