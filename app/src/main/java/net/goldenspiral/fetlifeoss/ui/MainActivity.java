/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2015 <a href="https://bitbucket.org/malachid/fetlife-oss/src/default/Contributors.md?at=default">FetLife-OSS Contributors</a>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package net.goldenspiral.fetlifeoss.ui;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import net.goldenspiral.fetlifeoss.FetLife;
import net.goldenspiral.fetlifeoss.R;
import net.goldenspiral.fetlifeoss.databinding.ActivityMainBinding;
import net.goldenspiral.fetlifeoss.sync.PeriodicSync;
import net.goldenspiral.fetlifeoss.ui.components.FetLifeDatabinder;
import net.goldenspiral.fetlifeoss.ui.components.Router;
import net.goldenspiral.fetlifeoss.ui.fragments.EmptyFragment;
import net.goldenspiral.fetlifeoss.ui.fragments.FetLifeFragment;
import net.goldenspiral.fetlifeoss.ui.fragments.LoginFragment;
import net.goldenspiral.fetlifeoss.ui.fragments.NavigationFragment;

import rx.Subscriber;

import static android.support.v4.widget.DrawerLayout.LOCK_MODE_LOCKED_CLOSED;
import static android.support.v4.widget.DrawerLayout.LOCK_MODE_UNLOCKED;
import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * Created by malachi on 7/24/15.
 */
public class MainActivity
extends AppCompatActivity
implements Toolbar.OnMenuItemClickListener, View.OnClickListener
{
    private static final String TAG = MainActivity.class.getSimpleName();
    private ActionBarDrawerToggle mDrawerToggle;
    private ActivityMainBinding binding;
    private NavigationFragment navigation;
    private Router router = new Router(this);

    public ActivityMainBinding getBinding(){return binding;}

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // @TODO AndroidStudio shows an error for FetLifeDatabinder - see it for details
        DataBindingUtil.setDefaultComponent(FetLifeDatabinder.INSTANCE);
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        if (savedInstanceState != null) {
            // don't duplicate / overlap... @TODO any state to restore?
            return;
        }

        swapPage(EmptyFragment.newInstance(), false);

        // Can't currently using binding across fragment-in-xml declarations
        navigation = (NavigationFragment) getSupportFragmentManager().findFragmentById(R.id.navigation_fragment);

        binding.toolbar.setOnMenuItemClickListener(this);
        binding.toolbar.setLogo(R.mipmap.ic_launcher);
        setSupportActionBar(binding.toolbar);
        binding.toolbar.setNavigationOnClickListener(this);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        mDrawerToggle = new ActionBarDrawerToggle(this, binding.drawerLayout, binding.toolbar, R.string.open_drawer, R.string.close_drawer);
        binding.drawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        // If they logout, we (will) remove the password, and it will redirect to the Login
        FetLife.getAccountManager().validate().subscribe(new Subscriber<Void>() {
            @Override
            public void onCompleted() {
                swapPage(EmptyFragment.newInstance());
                setDrawerLocked(false);
                router.handleLaunchUri(getIntent().getData(), false);

                // @TODO temp
                sendBroadcast(PeriodicSync.createIntent(MainActivity.this, true));
            }

            @Override
            public void onError(Throwable e) {
                // @TODO show error in UI
                swapPage(LoginFragment.newInstance());
                setDrawerLocked(true);

                // @TODO temp
                sendBroadcast(PeriodicSync.createIntent(MainActivity.this, false));
            }

            @Override
            public void onNext(Void aVoid) {
                // no-op
            }
        });
    }

    public void swapPage(FetLifeFragment fragment)
    {
        swapPage(fragment, true);
    }

    private void swapPage(FetLifeFragment fragment, boolean replaceFragment)
    {
        binding.drawerLayout.closeDrawers();

        Bundle args = fragment.getArguments();
        if(args == null) {
            fragment.setArguments(getIntent().getExtras());
        }else{
            args.putAll(getIntent().getExtras());
            fragment.setArguments(args);
        }
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if(replaceFragment)
            transaction.replace(R.id.activity_container, fragment);
        else
            transaction.add(R.id.activity_container, fragment);

        transaction.commit();
        int titleId = fragment.getTitle();
        binding.toolbarTitle.setText(titleId == -1 ?
                getResources().getText(R.string.app_name) :
                getResources().getText(titleId));
    }

    public void setDrawerLocked(boolean locked)
    {
        binding.drawerLayout.setDrawerLockMode(locked ? LOCK_MODE_LOCKED_CLOSED : LOCK_MODE_UNLOCKED);
        navigation.getRootView().setVisibility(locked ? GONE : VISIBLE);
//        toolbarLayout.setVisibility(locked ? GONE : VISIBLE);
        mDrawerToggle.syncState();
    }


    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {

    }

    /**
     * This method will be invoked when a menu item is clicked if the item itself did
     * not already handle the event.
     *
     * @param item {@link MenuItem} that was clicked
     * @return <code>true</code> if the event was handled, <code>false</code> otherwise.
     */
    @Override
    public boolean onMenuItemClick(MenuItem item) {
        return false;
    }

    /**
     * Dispatch onResume() to fragments.  Note that for better inter-operation
     * with older versions of the platform, at the point of this call the
     * fragments attached to the activity are <em>not</em> resumed.  This means
     * that in some cases the previous state may still be saved, not allowing
     * fragment transactions that modify the state.  To correctly interact
     * with fragments in their proper state, you should instead override
     * {@link #onResumeFragments()}.
     */
    @Override
    protected void onResume() {
        super.onResume();
        router.onResume();
    }



    /**
     * Take care of popping the fragment back stack or finishing the activity
     * as appropriate.
     */
    @Override
    public void onBackPressed() {
        if (binding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            binding.drawerLayout.closeDrawer(GravityCompat.START);
            return;
        }

        FetLifeFragment fragment = (FetLifeFragment) getSupportFragmentManager().findFragmentById(R.id.activity_container);
        if(fragment != null) {
            fragment.onBackPressed();
            return;
        }

        super.onBackPressed();
    }

    /**
     * Dispatch onPause() to fragments.
     */
    @Override
    protected void onPause() {
        router.onPause();
        super.onPause();
    }

}
