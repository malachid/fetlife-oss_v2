package net.goldenspiral.fetlifeoss.ui.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import android.widget.ListView;

import net.goldenspiral.fetlifeoss.FetLife;
import net.goldenspiral.fetlifeoss.R;
import net.goldenspiral.fetlifeoss.rest.model.FriendsPage;
import net.goldenspiral.fetlifeoss.rest.model.User;
import net.goldenspiral.fetlifeoss.ui.components.DataBoundViewHolder;
import net.goldenspiral.fetlifeoss.ui.fragments.FriendsFragment;
import net.goldenspiral.fetlifeoss.util.Log;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by malachi on 10/12/15.
 */
public class FriendsAdapter
extends RecyclerView.Adapter<DataBoundViewHolder>
{
    private static final String TAG = FriendsAdapter.class.getSimpleName();
    private ArrayList<User> friends = new ArrayList<>();
    private FriendsFragment fragment;
    private int requestMoreAt = 0;
    private AtomicInteger currentPage = new AtomicInteger(1);  // 0 will give 500 server error
    private AtomicBoolean requestPending = new AtomicBoolean(false);
    private User friendsOf;

    public FriendsAdapter(FriendsFragment fragment, User user)
    {
        this.fragment = fragment;
        this.friendsOf = user;
        requestPage(currentPage.get());
    }

    private void requestPage(final int pageNum)
    {
        if(requestPending.getAndSet(true))
            return;

        currentPage.set(pageNum);
        FetLife.getServer().friends(friendsOf.getId(), pageNum)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Subscriber<FriendsPage>() {
                @Override
                public void onCompleted() {
                    requestPending.getAndSet(false);
                }

                @Override
                public void onError(Throwable e) {
                    // @TODO handle in UI
                    Log.d(TAG, "Error in requestPage: "  + e.getMessage());
                    requestPending.getAndSet(false);
                }

                @Override
                public void onNext(FriendsPage friendsPage) {
                    int position = friends.size();
                    int count  = friendsPage.getFriends().size();
                    friends.addAll(friendsPage.getFriends());
                    if(count == 0)
                        requestMoreAt = Integer.MAX_VALUE;
                    else
                        requestMoreAt = (int) (((float)friends.size()) * 0.8);

                    notifyItemRangeInserted(position, count);
                }
            });
    }

    /**
     * Called when RecyclerView needs a new {@link ViewHolder} of the given type to represent
     * an item.
     * <p/>
     * This new ViewHolder should be constructed with a new View that can represent the items
     * of the given type. You can either create a new View manually or inflate it from an XML
     * layout file.
     * <p/>
     * The new ViewHolder will be used to display items of the adapter using
     * {@link #onBindViewHolder(ViewHolder, int, List)}. Since it will be re-used to display
     * different items in the data set, it is a good idea to cache references to sub views of
     * the View to avoid unnecessary {@link View#findViewById(int)} calls.
     *
     * @param parent   The ViewGroup into which the new View will be added after it is bound to
     *                 an adapter position.
     * @param viewType The view type of the new View.
     * @return A new ViewHolder that holds a View of the given view type.
     * @see #getItemViewType(int)
     * @see #onBindViewHolder(ViewHolder, int)
     */
    @Override
    public DataBoundViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return DataBoundViewHolder.create(parent, viewType);
    }

    /**
     * Called by RecyclerView to display the data at the specified position. This method should
     * update the contents of the {@link ViewHolder#itemView} to reflect the item at the given
     * position.
     * <p/>
     * Note that unlike {@link ListView}, RecyclerView will not call this method
     * again if the position of the item changes in the data set unless the item itself is
     * invalidated or the new position cannot be determined. For this reason, you should only
     * use the <code>position</code> parameter while acquiring the related data item inside
     * this method and should not keep a copy of it. If you need the position of an item later
     * on (e.g. in a click listener), use {@link ViewHolder#getAdapterPosition()} which will
     * have the updated adapter position.
     * <p/>
     * Override {@link #onBindViewHolder(ViewHolder, int, List)} instead if Adapter can
     * handle effcient partial bind.
     *
     * @param holder   The ViewHolder which should be updated to represent the contents of the
     *                 item at the given position in the data set.
     * @param position The position of the item within the adapter's data set.
     */
    @Override
    public void onBindViewHolder(DataBoundViewHolder holder, int position) {

        // @TODO temp
//        User friend = friends.get(position);
//        Log.d(TAG, "binding to %s [%d]", friend.getNickname(), friend.getId());

        holder.bindTo(friends.get(position));
        if(position > requestMoreAt && !requestPending.get())
            requestPage(currentPage.get() + 1);
    }

    /**
     * Returns the total number of items in the data set hold by the adapter.
     *
     * @return The total number of items in this adapter.
     */
    @Override
    public int getItemCount() {
        return friends.size();
    }

    /**
     * Return the view type of the item at <code>position</code> for the purposes
     * of view recycling.
     * <p/>
     * <p>The default implementation of this method returns 0, making the assumption of
     * a single view type for the adapter. Unlike ListView adapters, types need not
     * be contiguous. Consider using id resources to uniquely identify item view types.
     *
     * @param position position to query
     * @return integer value identifying the type of the view needed to represent the item at
     * <code>position</code>. Type codes need not be contiguous.
     */
    @Override
    public int getItemViewType(int position) {
        return R.layout.list_user_type;
    }
}
