/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 <a href="https://bitbucket.org/malachid/fetlife-oss/src/default/Contributors.md?at=default">FetLife-OSS Contributors</a>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package net.goldenspiral.fetlifeoss.ui.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import android.widget.ListView;

import net.goldenspiral.fetlifeoss.R;
import net.goldenspiral.fetlifeoss.rest.model.UserGroups;
import net.goldenspiral.fetlifeoss.ui.components.DataBoundViewHolder;
import net.goldenspiral.fetlifeoss.util.Log;

/**
 * Created by malachi on 10/10/15.
 */
public class UserGroupsAdapter
extends RecyclerView.Adapter<DataBoundViewHolder>
{
    private static final String TAG = UserGroupsAdapter.class.getSimpleName();
    private UserGroups userGroups;
    private String leadHeader, memberHeader;
    private int leaderSize, memberSize = 0;
    private int leaderHeaderPosition = 0;
    private int memberHeaderPosition = 1;
    private int totalCount = 2;

    public UserGroupsAdapter(String leadHeader, String memberHeader)
    {
        this.leadHeader = leadHeader;
        this.memberHeader = memberHeader;
    }

    public void setGroups(UserGroups groups)
    {
        this.userGroups = groups;
        leaderSize = groups.getGroupsILead().size();
        memberSize = groups.getGroupsImIn().size();
        memberHeaderPosition = leaderSize + 1;
        totalCount = leaderSize + memberSize + 2;
        Log.d(TAG, "Loaded %d/%d profile groups", leaderSize, memberSize);
        notifyDataSetChanged();
    }

    /**
     * Called when RecyclerView needs a new {@link RecyclerView.ViewHolder} of the given type to represent
     * an item.
     * <p/>
     * This new ViewHolder should be constructed with a new View that can represent the items
     * of the given type. You can either create a new View manually or inflate it from an XML
     * layout file.
     * <p/>
     * The new ViewHolder will be used to display items of the adapter using
     * {@link #onBindViewHolder(ViewHolder, int, List)}. Since it will be re-used to display
     * different items in the data set, it is a good idea to cache references to sub views of
     * the View to avoid unnecessary {@link View#findViewById(int)} calls.
     *
     * @param parent   The ViewGroup into which the new View will be added after it is bound to
     *                 an adapter position.
     * @param viewType The view type of the new View.
     * @return A new ViewHolder that holds a View of the given view type.
     * @see #getItemViewType(int)
     * @see #onBindViewHolder(ViewHolder, int)
     */
    @Override
    public DataBoundViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return DataBoundViewHolder.create(parent, viewType);
    }

    /**
     * Called by RecyclerView to display the data at the specified position. This method should
     * update the contents of the {@link ViewHolder#itemView} to reflect the item at the given
     * position.
     * <p/>
     * Note that unlike {@link ListView}, RecyclerView will not call this method
     * again if the position of the item changes in the data set unless the item itself is
     * invalidated or the new position cannot be determined. For this reason, you should only
     * use the <code>position</code> parameter while acquiring the related data item inside
     * this method and should not keep a copy of it. If you need the position of an item later
     * on (e.g. in a click listener), use {@link ViewHolder#getAdapterPosition()} which will
     * have the updated adapter position.
     * <p/>
     * Override {@link #onBindViewHolder(ViewHolder, int, List)} instead if Adapter can
     * handle effcient partial bind.
     *
     * @param holder   The ViewHolder which should be updated to represent the contents of the
     *                 item at the given position in the data set.
     * @param position The position of the item within the adapter's data set.
     */
    @Override
    public void onBindViewHolder(DataBoundViewHolder holder, int position) {
        if(position == leaderHeaderPosition)
        {
            holder.bindTo(leadHeader);
        }else if(position < memberHeaderPosition){
            holder.bindTo(userGroups.getGroupsILead().get(position - (leaderHeaderPosition + 1)));
        }else if(position == memberHeaderPosition){
            holder.bindTo(memberHeader);
        }else{
            holder.bindTo(userGroups.getGroupsImIn().get(position - (memberHeaderPosition + 1)));
        }
    }

    /**
     * Returns the total number of items in the data set hold by the adapter.
     *
     * @return The total number of items in this adapter.
     */
    @Override
    public int getItemCount() {
        return totalCount;
    }

    public int getItemViewType(int position) {
        if(position == leaderHeaderPosition || position == memberHeaderPosition)
            return R.layout.list_header_type;

        return R.layout.list_groupref_type;
    }
}
