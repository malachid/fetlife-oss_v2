/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 <a href="https://bitbucket.org/malachid/fetlife-oss/src/default/Contributors.md?at=default">FetLife-OSS Contributors</a>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package net.goldenspiral.fetlifeoss.ui.fragments.profile;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.MenuRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.goldenspiral.fetlifeoss.FetLife;
import net.goldenspiral.fetlifeoss.R;
import net.goldenspiral.fetlifeoss.databinding.FragmentProfileGroupsBinding;
import net.goldenspiral.fetlifeoss.rest.model.UserGroups;
import net.goldenspiral.fetlifeoss.ui.adapters.UserGroupsAdapter;
import net.goldenspiral.fetlifeoss.ui.fragments.FetLifeFragment;
import net.goldenspiral.fetlifeoss.util.Log;

import org.solovyev.android.views.llm.LinearLayoutManager;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by malachi on 10/10/15.
 */
public class ProfileGroupsFragment
extends FetLifeFragment<FragmentProfileGroupsBinding>
{
    private static final String TAG = ProfileGroupsFragment.class.getSimpleName();

    public static ProfileGroupsFragment newInstance()
    {
        return new ProfileGroupsFragment();
    }

    /**
     * Default constructor.  <strong>Every</strong> fragment must have an
     * empty constructor, so it can be instantiated when restoring its
     * activity's state.  It is strongly recommended that subclasses do not
     * have other constructors with parameters, since these constructors
     * will not be called when the fragment is re-instantiated; instead,
     * arguments can be supplied by the caller with {@link #setArguments}
     * and later retrieved by the Fragment with {@link #getArguments}.
     * <p/>
     * <p>Applications should generally not implement a constructor.  The
     * first place application code an run where the fragment is ready to
     * be used is in {@link Fragment#onAttach(Activity)}, the point where the fragment
     * is actually associated with its activity.  Some applications may also
     * want to implement {@link #onInflate} to retrieve attributes from a
     * layout resource, though should take care here because this happens for
     * the fragment is attached to its activity.
     */
    public ProfileGroupsFragment() {
        super();
    }

    /**
     * Title resource id.
     *
     * @return title resource identifier
     */
    @Override
    public int getTitle() {
        return R.string.profile_title;
    }

    protected @MenuRes
    int getOptionMenuResId()
    {
        return R.menu.profile;
    }

    /**
     * Called to have the fragment instantiate its user interface view.
     * This is optional, and non-graphical fragments can return null (which
     * is the default implementation).  This will be called between
     * {@link #onCreate(Bundle)} and {@link #onActivityCreated(Bundle)}.
     * <p/>
     * <p>If you return a View from here, you will later be called in
     * {@link #onDestroyView} when the view is being released.
     *
     * @param inflater           The LayoutInflater object that can be used to inflate
     *                           any views in the fragment,
     * @param container          If non-null, this is the parent view that the fragment's
     *                           UI should be attached to.  The fragment should not add the view itself,
     *                           but this can be used to generate the LayoutParams of the view.
     * @param savedInstanceState If non-null, this fragment is being re-constructed
     *                           from a previous saved state as given here.
     * @return Return the View for the fragment's UI, or null.
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setRootBinding(bind(inflater, container, R.layout.fragment_profile_groups));
        final RecyclerView recyclerView = getRootBinding().recycle;
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        final UserGroupsAdapter adapter = new UserGroupsAdapter(
                getResources().getString(R.string.profile_groups_ilead),
                getResources().getString(R.string.profile_groups_imin));
        recyclerView.setAdapter(adapter);

        // @TODO we need a call to get the groups for the specified user....
        FetLife.getServer().myGroups()
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Subscriber<UserGroups>() {
                @Override
                public void onCompleted() {
                    // no-op
                }

                @Override
                public void onError(Throwable e) {
                    Log.e(TAG, e, "Error getting my groups");
                    // @TODO handle error in UI
                }

                @Override
                public void onNext(UserGroups userGroups) {
                    adapter.setGroups(userGroups);
                }
            });
        setHasOptionsMenu(true);
        return getRootView();
    }
}
