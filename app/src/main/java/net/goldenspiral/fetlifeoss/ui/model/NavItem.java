/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 <a href="https://bitbucket.org/malachid/fetlife-oss/src/default/Contributors.md?at=default">FetLife-OSS Contributors</a>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package net.goldenspiral.fetlifeoss.ui.model;

import android.databinding.BaseObservable;
import android.graphics.drawable.Drawable;
import android.support.annotation.IdRes;
import android.view.MenuItem;
import android.view.View;

import net.goldenspiral.fetlifeoss.BR;
import net.goldenspiral.fetlifeoss.FetLife;
import net.goldenspiral.fetlifeoss.bus.NavItemClicked;

/**
 * Created by malachi on 10/11/15.
 */
public class NavItem
extends BaseObservable
{
    @IdRes private int clickId;
    private Drawable icon;
    private CharSequence title;
    private long count = 0;

    public NavItem(@IdRes int clickId)
    {
        this.clickId = clickId;
    }

    public NavItem(MenuItem source)
    {
        this.clickId = source.getItemId();
        this.icon = source.getIcon();
        this.title = source.getTitle();
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }


    public int getClickId() {
        return clickId;
    }

    public void setClickId(int clickId) {
        this.clickId = clickId;
        notifyPropertyChanged(BR.data);
    }

    public void onClick(View view)
    {
        FetLife.getBus().post(new NavItemClicked(this));
    }

    public boolean equals(Object other)
    {
        if(other == null) return false;
        if(other instanceof NavItem)
            return clickId == ((NavItem)other).getClickId();

        if(other instanceof Number)
            return clickId == ((Number)other).longValue();

        return false;
    }

    public Drawable getIcon() {
        return icon;
    }

    public void setIcon(Drawable icon) {
        this.icon = icon;
    }

    public CharSequence getTitle() {
        return title;
    }

    public void setTitle(CharSequence title) {
        this.title = title;
    }
}
