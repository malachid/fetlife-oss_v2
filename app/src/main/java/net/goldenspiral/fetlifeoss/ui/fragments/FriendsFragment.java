package net.goldenspiral.fetlifeoss.ui.fragments;

import android.os.Bundle;
import android.support.annotation.MenuRes;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.goldenspiral.fetlifeoss.R;
import net.goldenspiral.fetlifeoss.databinding.FragmentFriendsBinding;
import net.goldenspiral.fetlifeoss.rest.model.User;
import net.goldenspiral.fetlifeoss.ui.adapters.FriendsAdapter;

import org.solovyev.android.views.llm.LinearLayoutManager;

/**
 * Created by malachi on 10/12/15.
 */
public class FriendsFragment
extends FetLifeFragment<FragmentFriendsBinding>
{
    private static final String TAG = FriendsFragment.class.getSimpleName();
    private static final String ARG_FRIENDSOF = FriendsFragment.class.getName() + ":friendsof";
    private User user;

    public static FriendsFragment newInstance(User friendsOf)
    {
        FriendsFragment fragment = new FriendsFragment();
        Bundle options = new Bundle();
        fragment.addToBundle(options, ARG_FRIENDSOF, friendsOf);
        fragment.setArguments(options);
        return fragment;
    }

    /**
     * Title resource id.
     *
     * @return title resource identifier
     */
    @Override
    public int getTitle() {
        return R.string.friends_title;
    }

    protected @MenuRes
    int getOptionMenuResId()
    {
        return R.menu.friends;
    }

    /**
     * Called to do initial creation of a fragment.  This is called after
     * {@link #onAttach(Activity)} and before
     * {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)}.
     * <p/>
     * <p>Note that this can be called while the fragment's activity is
     * still in the process of being created.  As such, you can not rely
     * on things like the activity's content view hierarchy being initialized
     * at this point.  If you want to do work once the activity itself is
     * created, see {@link #onActivityCreated(Bundle)}.
     *
     * @param savedInstanceState If the fragment is being re-created from
     *                           a previous saved state, this is the state.
     */
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        user = getFromBundle(getArguments(), ARG_FRIENDSOF, User.class);
    }

    /**
     * Called to have the fragment instantiate its user interface view.
     * This is optional, and non-graphical fragments can return null (which
     * is the default implementation).  This will be called between
     * {@link #onCreate(Bundle)} and {@link #onActivityCreated(Bundle)}.
     * <p/>
     * <p>If you return a View from here, you will later be called in
     * {@link #onDestroyView} when the view is being released.
     *
     * @param inflater           The LayoutInflater object that can be used to inflate
     *                           any views in the fragment,
     * @param container          If non-null, this is the parent view that the fragment's
     *                           UI should be attached to.  The fragment should not add the view itself,
     *                           but this can be used to generate the LayoutParams of the view.
     * @param savedInstanceState If non-null, this fragment is being re-constructed
     *                           from a previous saved state as given here.
     * @return Return the View for the fragment's UI, or null.
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setRootBinding(bind(inflater, container, R.layout.fragment_friends));
        final RecyclerView recyclerView = getRootBinding().recycle;
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(new FriendsAdapter(this, user));
        setHasOptionsMenu(true);
        return getRootView();
    }
}
