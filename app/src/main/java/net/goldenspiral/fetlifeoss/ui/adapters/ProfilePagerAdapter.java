package net.goldenspiral.fetlifeoss.ui.adapters;

import android.content.res.TypedArray;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentStatePagerAdapter;

import net.goldenspiral.fetlifeoss.R;
import net.goldenspiral.fetlifeoss.ui.fragments.EmptyFragment;
import net.goldenspiral.fetlifeoss.ui.fragments.profile.ProfileGroupsFragment;

/**
 * Created by malachi on 10/10/15.
 */
public class ProfilePagerAdapter
extends FragmentStatePagerAdapter
{
    private int[] keys;
    private String[] titles;

    public ProfilePagerAdapter(FragmentActivity context) {
        super(context.getSupportFragmentManager());
        TypedArray array = context.getResources().obtainTypedArray(R.array.profile_tabs);
        int length = array.length();
        keys = new int[length];
        titles = new String[length];
        for(int i=0; i<length; i++)
        {
            keys[i] = array.getResourceId(i, -1);
            titles[i] = keys[i] == -1 ? "" : context.getString(keys[i]);
        }
        array.recycle();
        array = null;
    }

    /**
     * This method may be called by the ViewPager to obtain a title string
     * to describe the specified page. This method may return null
     * indicating no title for this page. The default implementation returns
     * null.
     *
     * @param position The position of the title requested
     * @return A title for the requested page
     */
    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
    }

    /**
     * Return the Fragment associated with a specified position.
     *
     * @param position
     */
    @Override
    public Fragment getItem(int position) {
        switch (keys[position])
        {
            case R.string.profile_tab_groups:
                return ProfileGroupsFragment.newInstance();

            case R.string.profile_tab_aboutme:
            case R.string.profile_tab_recent:
            case R.string.profile_tab_wall:
            case R.string.profile_tab_events:
            case R.string.profile_tab_fetishes:
            case R.string.profile_tab_websites:
            default:
                return EmptyFragment.newInstance();
        }
    }

    /**
     * Return the number of views available.
     */
    @Override
    public int getCount() {
        return titles.length;
    }
}
