package net.goldenspiral.fetlifeoss.ui.components;

import android.content.Intent;
import android.net.Uri;

import com.squareup.otto.Subscribe;

import net.goldenspiral.fetlifeoss.FetLife;
import net.goldenspiral.fetlifeoss.R;
import net.goldenspiral.fetlifeoss.bus.GenericClickEvent;
import net.goldenspiral.fetlifeoss.bus.NavItemClicked;
import net.goldenspiral.fetlifeoss.ui.MainActivity;
import net.goldenspiral.fetlifeoss.ui.fragments.FriendsFragment;
import net.goldenspiral.fetlifeoss.ui.fragments.profile.ProfileFragment;
import net.goldenspiral.fetlifeoss.ui.model.NavItem;
import net.goldenspiral.fetlifeoss.util.Log;
import net.goldenspiral.fetlifeoss.util.PreOSSInterop;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import rx.Subscriber;

/**
 * Created by malachi on 10/12/15.
 */
public class Router
{
    private static final String TAG = Router.class.getSimpleName();
    private AtomicBoolean registeredWithBus = new AtomicBoolean(false);
    private MainActivity activity;

    public Router(MainActivity activity)
    {
        this.activity = activity;
        registeredWithBus.getAndSet(true);
        FetLife.getBus().register(this);
    }

    public void onPause()
    {
        if(registeredWithBus.getAndSet(false))
            FetLife.getBus().unregister(this);
    }

    public void onResume()
    {
        if(!registeredWithBus.getAndSet(true))
            FetLife.getBus().register(this);

    }

    @Subscribe
    public void onGenericClick(GenericClickEvent event)
    {
        handleLaunchUri(Uri.parse(event.getUrl()), true);
    }

    public void handleLaunchUri(final Uri uri, boolean launchIfUnhandled)
    {
        if(uri == null) return;
        List<String> segments = uri.getPathSegments();
        if(segments.size() == 0) return;
        switch(segments.get(0))
        {
            case "users":
                handleUserLaunchUri(uri, segments);
                break;
            default:
                // Maybe we are launching a browser, email, etc...
                if(launchIfUnhandled) launch(uri);
                break;
        }
    }

    private void handleUserLaunchUri(final Uri uri, final List<String> segments)
    {
        if(segments.size() == 2)
        {
            ProfileFragment.newInstance(Integer.parseInt(segments.get(1)))
                .subscribe(new Subscriber<ProfileFragment>() {
                    @Override
                    public void onCompleted() {
                        // no-op
                    }

                    @Override
                    public void onError(Throwable e) {
                        // @TODO show errors in UI
                        Log.d(TAG, "Error handling uri: " + uri.getPath());
                    }

                    @Override
                    public void onNext(ProfileFragment profileFragment) {
                        activity.swapPage(profileFragment);
                    }
                });
        }// @TODO else....
    }

    private void launch(Uri uri)
    {
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(intent);
    }

    @Subscribe
    public void onNavigationItemSelected(NavItemClicked event) {
        NavItem selectedItem = event.getItem();
        activity.getBinding().drawerLayout.closeDrawers();
        switch(selectedItem.getClickId())
        {
            case R.id.nav_feed:
                PreOSSInterop.launch(activity, "/feed/");
                break;

            case R.id.nav_groups:
                PreOSSInterop.launch(activity, "/groups/");
                break;

            case R.id.nav_events:
                PreOSSInterop.launch(activity, "/events/");
                break;

            case R.id.nav_knp:
                PreOSSInterop.launch(activity, "/explore/");
                break;

            case R.id.nav_places:
                PreOSSInterop.launch(activity, "/places/");
                break;

            case R.id.nav_writing:
                PreOSSInterop.launch(activity, "/writings/");
                break;

            case R.id.nav_fetishes:
                PreOSSInterop.launch(activity, "/fetishes/");
                break;

            case R.id.nav_videos:
                PreOSSInterop.launch(activity, "/videos/.*");
                break;

            case R.id.nav_inbox:
                PreOSSInterop.launch(activity, "/inbox/");
                break;

            case R.id.nav_requests:
                PreOSSInterop.launch(activity, "/requests/");
                break;

            case R.id.nav_ats:
                PreOSSInterop.launch(activity, "/ats/");
                break;

            case R.id.nav_profile:
                activity.swapPage(ProfileFragment.newInstance(FetLife.getAccountManager().getCurrentUser()));
                break;

            case R.id.nav_friends:
                activity.swapPage(FriendsFragment.newInstance(FetLife.getAccountManager().getCurrentUser()));
                break;

            case R.id.nav_upload:
                PreOSSInterop.launch(activity, "/uploadvid/");
                break;

            case R.id.nav_write:
                PreOSSInterop.launch(activity, "/newwriting/");
                break;

            case R.id.nav_editprofile:
                PreOSSInterop.launch(activity, "/editprofile/");
                break;

            case R.id.nav_settings:
                PreOSSInterop.launch(activity, "/settings/");
                break;

            case R.id.nav_share:
            case R.id.nav_help:
            default:
                break;
        }
    }
}
