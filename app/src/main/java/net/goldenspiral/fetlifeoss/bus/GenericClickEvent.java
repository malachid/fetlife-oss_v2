package net.goldenspiral.fetlifeoss.bus;

/**
 * Created by malachi on 10/12/15.
 */
public class GenericClickEvent
{
    private String url;

    public GenericClickEvent(String url)
    {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }
}
