/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 <a href="https://bitbucket.org/malachid/fetlife-oss/src/default/Contributors.md?at=default">FetLife-OSS Contributors</a>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package net.goldenspiral.fetlifeoss.cookie;

import android.accounts.Account;
import android.content.Context;
import android.support.annotation.NonNull;

import com.squareup.otto.Subscribe;

import net.goldenspiral.fetlifeoss.FetLife;
import net.goldenspiral.fetlifeoss.auth.FetLifeAccountManager;
import net.goldenspiral.fetlifeoss.auth.NoAccountsException;
import net.goldenspiral.fetlifeoss.bus.LoginEvent;
import net.goldenspiral.fetlifeoss.util.Log;

import java.net.CookieStore;
import java.net.HttpCookie;
import java.net.URI;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by malachi on 8/11/15.
 */
public class MultiUserCookieStore implements CookieStore
{
    private static final String TAG = MultiUserCookieStore.class.getSimpleName();
    private ConcurrentHashMap<Long,PersistentCookieStore> userStores;
    private FetLifeAccountManager acctMgr;
    private Context appContext;
    private long oldId = 0;

    /**
     * Constructs a new instance of {@code Object}.
     */
    public MultiUserCookieStore(final Context context) {
        super();
        appContext = context.getApplicationContext();
        acctMgr = FetLife.getAccountManager();
        try {
            Account[] accounts = acctMgr.getAccounts();
            userStores = new ConcurrentHashMap<>(accounts.length);
            for(Account account : accounts)
                setupAccount(account);
        } catch (NoAccountsException e) {
            userStores = new ConcurrentHashMap<>();
        }
        FetLife.getBus().register(this);
    }

    @NonNull
    private PersistentCookieStore setupAccount(final Account account)
    {
        long id = account == null ? 0 : acctMgr.getUser(account).getId();
        PersistentCookieStore store = userStores.get(id);
        if(store == null) {
            store = new PersistentCookieStore(appContext, id);
            if(oldId == 0 && id != 0)
            {
                PersistentCookieStore oldStore = userStores.get(oldId);
                store.migrate(oldStore);
            }
            userStores.put(id, store);
        }

        oldId = id;
        Log.d(TAG, "Returning cookie store for user %d", id);
        return store;
    }

    @NonNull
    private PersistentCookieStore getCurrentStore()
    {
        return setupAccount(acctMgr.getCurrentAccount());
    }

    @Subscribe
    public void login(LoginEvent event)
    {
        Account acct = event == null ? null : event.getAccount();
        Log.d(TAG, "Login from %s", acct == null ? null : acct.name);
        setupAccount(acct);
    }

    /**
     * Saves a HTTP cookie to this store. This is called for every incoming HTTP
     * response.
     * <p/>
     * A cookie may or may not has an associated URI. If not, the cookie's
     * domain and path attribute will show cradleland. If there is an
     * associated URI and no domain and path attribute are speicifed for the
     * cookie, the given URI will indicate where this cookie comes from.
     * <p/>
     * If a cookie corresponding to the given URI already exists, then it is
     * replaced with the new one.
     *
     * @param uri    the uri associated with the specified cookie. A null value
     *               indicates the cookie is not associated with a URI
     * @param cookie
     */
    @Override
    public void add(URI uri, HttpCookie cookie) {
        getCurrentStore().add(uri, cookie);
    }

    /**
     * Retrieves cookies that match the specified URI. Return not expired cookies.
     * For every outgoing HTTP request, this method will be called.
     *
     * @param uri the uri this cookie associated with. If null, this cookie will
     *            not be associated with an URI
     * @return an immutable list of HttpCookies, return empty list if no cookies
     * match the given URI
     * @throws NullPointerException if uri is null
     */
    @Override
    public List<HttpCookie> get(URI uri) {
        return getCurrentStore().get(uri);
    }

    /**
     * Get all cookies in cookie store which are not expired.
     *
     * @return an empty list if there's no http cookie in store, or an immutable
     * list of cookies
     */
    @Override
    public List<HttpCookie> getCookies() {
        return getCurrentStore().getCookies();
    }

    /**
     * Get a set of URIs, which is composed of associated URI with all the
     * cookies in the store.
     *
     * @return zero-length list if no cookie in the store is associated with any
     * URIs, otherwise an immutable list of URIs.
     */
    @Override
    public List<URI> getURIs() {
        return getCurrentStore().getURIs();
    }

    /**
     * Remove the specified cookie from the store.
     *
     * @param uri    the uri associated with the specified cookie. If the cookie is
     *               not associated with an URI when added, uri should be null;
     *               otherwise the uri should be non-null.
     * @param cookie the cookie to be removed
     * @return true if the specified cookie is contained in this store and
     * removed successfully
     */
    @Override
    public boolean remove(URI uri, HttpCookie cookie) {
        return getCurrentStore().remove(uri, cookie);
    }

    /**
     * Clear this cookie store.
     *
     * @return true if any cookies were removed as a result of this call.
     */
    @Override
    public boolean removeAll() {
        return getCurrentStore().removeAll();
    }
}
