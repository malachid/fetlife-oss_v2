/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 <a href="https://bitbucket.org/malachid/fetlife-oss/src/default/Contributors.md?at=default">FetLife-OSS Contributors</a>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package net.goldenspiral.fetlifeoss.auth;

import android.accounts.Account;
import android.accounts.AccountAuthenticatorResponse;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.google.gson.Gson;
import com.squareup.otto.Produce;

import net.goldenspiral.fetlifeoss.FetLife;
import net.goldenspiral.fetlifeoss.R;
import net.goldenspiral.fetlifeoss.bus.AccountCreatedEvent;
import net.goldenspiral.fetlifeoss.bus.LoginEvent;
import net.goldenspiral.fetlifeoss.rest.model.Session;
import net.goldenspiral.fetlifeoss.rest.model.User;
import net.goldenspiral.fetlifeoss.util.gson.GsonUtil;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subjects.ReplaySubject;

/**
 * Created by malachi on 8/8/15.
 */
public class FetLifeAccountManager
{
    private static final String TAG = FetLifeAccountManager.class.getSimpleName();
    private static final String PREF_CURRENT_ACCOUNT = TAG + ":current_account";
    private static final String USER_DATA = "json";
    private Context appContext;
    private AccountManager accountManager;
    private Gson gson;
    private String accountType;
    private Account currentAccount;

    public FetLifeAccountManager(Context context)
    {
        this.appContext = context.getApplicationContext();
        accountManager = AccountManager.get(appContext);
        gson = GsonUtil.instance
            .getBuilder()
            .setPrettyPrinting()
            .create();
        accountType = appContext.getResources().getString(R.string.account_type);

        final String currentName = FetLife.getPreferences().getString(PREF_CURRENT_ACCOUNT, null);
        if(currentName != null)
        {
            try{
                for(Account account : getAccounts())
                {
                    if(! account.name.equals(currentName)) continue;
                    setCurrentAccount(account);
                    break;
                }
            } catch (NoAccountsException e) {
                // no-op
            }
        }
    }

    @Produce
    public LoginEvent lastLogin()
    {
        return new LoginEvent(currentAccount);
    }

    public Account getCurrentAccount()
    {
        return currentAccount;
    }

    public void setCurrentAccount(Account account)
    {
        if(currentAccount != null)
        {
            SharedPreferences.Editor editor = FetLife.getPreferences().edit();
            editor.putString(PREF_CURRENT_ACCOUNT, account == null ? null : account.name);
            editor.apply();
        }
        this.currentAccount = account;
        FetLife.getBus().post(new LoginEvent(currentAccount));
    }

    public Account[] getAccounts()
    throws NoAccountsException
    {
        Account[] accts = accountManager.getAccountsByType(accountType);
        if(accts == null || accts.length == 0)
            throw new NoAccountsException();

        return accts;
    }

    public User getCurrentUser()
    {
        return getUser(getCurrentAccount());
    }

    public User getUser(final Account account)
    {
        if(account == null)
            return null;

        final String json = accountManager.getUserData(account, USER_DATA);
        return gson.fromJson(json, User.class);
    }

    public Observable<Account> createAccount(final Activity callingActivity, final User me, final String user, final String password)
    {
        ReplaySubject<Account> subject = ReplaySubject.create();

        // Create the account if necessary
        Account account = new Account(user, accountType);
        final Bundle extraData = new Bundle();
        extraData.putString(USER_DATA, gson.toJson(me));
        boolean accountCreated = accountManager.addAccountExplicitly(account, password, extraData);
        if(accountCreated) {
            FetLife.getBus().post(new AccountCreatedEvent(account));
        }else{
            // If we didn't create the account, at least make sure to update it
            accountManager.setPassword(account, password);
        }

        subject.onNext(account);
        setCurrentAccount(account);

        if(callingActivity != null)
        {
            // Check whether we were launched explicitly to create the account
            Bundle extras = callingActivity.getIntent().getExtras();
            if(extras != null && accountCreated)
            {
                AccountAuthenticatorResponse response = extras.getParcelable(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE);
                if(response != null)
                {
                    // Let them know we succeeded if so...
                    Bundle result = new Bundle();
                    result.putString(AccountManager.KEY_ACCOUNT_NAME, user);
                    result.putString(AccountManager.KEY_ACCOUNT_TYPE, accountType);
                    response.onResult(result);
                }
            }
        }

        return subject.asObservable();
    }

    public Observable<Void> validate()
    {
        final ReplaySubject<Void> replaySubject = ReplaySubject.create();
        try{
            getAccounts();
        } catch (NoAccountsException e) {
            replaySubject.onError(e);
            return replaySubject.asObservable();
        }

        FetLife.getServer().login(currentAccount.name, accountManager.getPassword(currentAccount), "true", "")
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Subscriber<Session>() {
                @Override
                public void onCompleted() {
                    replaySubject.onCompleted();
                }

                @Override
                public void onError(Throwable e) {
                    replaySubject.onError(e);
                }

                @Override
                public void onNext(Session session) {
                    replaySubject.onNext(null);
                }
            });

        return replaySubject.asObservable();
    }
}
