/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 <a href="https://bitbucket.org/malachid/fetlife-oss/src/default/Contributors.md?at=default">FetLife-OSS Contributors</a>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package net.goldenspiral.fetlifeoss.auth;

import android.accounts.AbstractAccountAuthenticator;
import android.accounts.Account;
import android.accounts.AccountAuthenticatorResponse;
import android.accounts.AccountManager;
import android.accounts.NetworkErrorException;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;

import net.goldenspiral.fetlifeoss.BuildConfig;
import net.goldenspiral.fetlifeoss.ui.MainActivity;

/**
 * Created by malachi on 8/8/15.
 */
public class AccountAuthenticatorService
extends Service
{
    public static final String TAG = AccountAuthenticatorService.class.getSimpleName();
    public static final String ACTION_LOGIN = String.format("%s.%s", BuildConfig.APPLICATION_ID, "LOGIN");
    private AuthenticatorImpl authenticator = null;

    public AccountAuthenticatorService()
    {
        super();
    }

    /**
     * Return the communication channel to the service.  May return null if
     * clients can not bind to the service.  The returned
     * {@link IBinder} is usually for a complex interface
     * that has been <a href="{@docRoot}guide/components/aidl.html">described using
     * aidl</a>.
     * <p/>
     * <p><em>Note that unlike other application components, calls on to the
     * IBinder interface returned here may not happen on the main thread
     * of the process</em>.  More information about the main thread can be found in
     * <a href="{@docRoot}guide/topics/fundamentals/processes-and-threads.html">Processes and
     * Threads</a>.</p>
     *
     * @param intent The Intent that was used to bind to this service,
     *               as given to {@link Context#bindService
     *               Context.bindService}.  Note that any extras that were included with
     *               the Intent at that point will <em>not</em> be seen here.
     * @return Return an IBinder through which clients can call on to the
     * service.
     */
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        if(android.accounts.AccountManager.ACTION_AUTHENTICATOR_INTENT.equals(intent.getAction()))
            return getAuthenticator().getIBinder();

        return null;
    }

    private AuthenticatorImpl getAuthenticator()
    {
        if(authenticator == null)
            authenticator = new AuthenticatorImpl(this);

        return authenticator;
    }

    private static class AuthenticatorImpl extends AbstractAccountAuthenticator
    {
        private Context appContext;

        public AuthenticatorImpl(Context context) {
            super(context);
            appContext = context.getApplicationContext();
        }

        /**
         * Returns a Bundle that contains the Intent of the activity that can be used to edit the
         * properties. In order to indicate success the activity should call response.setResult()
         * with a non-null Bundle.
         *
         * @param response    used to set the result for the request. If the Constants.INTENT_KEY
         *                    is set in the bundle then this response field is to be used for sending future
         *                    results if and when the Intent is started.
         * @param accountType the AccountType whose properties are to be edited.
         * @return a Bundle containing the result or the Intent to start to continue the request.
         * If this is null then the request is considered to still be active and the result should
         * sent later using response.
         */
        @Override
        public Bundle editProperties(AccountAuthenticatorResponse response, String accountType) {
            return null;
        }

        /**
         * Adds an account of the specified accountType.
         *
         * @param response         to send the result back to the AccountManager, will never be null
         * @param accountType      the type of account to add, will never be null
         * @param authTokenType    the type of auth token to retrieve after adding the account, may be null
         * @param requiredFeatures a String array of authenticator-specific features that the added
         *                         account must support, may be null
         * @param options          a Bundle of authenticator-specific options, may be null
         * @return a Bundle result or null if the result is to be returned via the response. The result
         * will contain either:
         * <ul>
         * <li> {@link AccountManager#KEY_INTENT}, or
         * <li> {@link AccountManager#KEY_ACCOUNT_NAME} and {@link AccountManager#KEY_ACCOUNT_TYPE} of
         * the account that was added, or
         * <li> {@link AccountManager#KEY_ERROR_CODE} and {@link AccountManager#KEY_ERROR_MESSAGE} to
         * indicate an error
         * </ul>
         * @throws NetworkErrorException if the authenticator could not honor the request due to a
         *                               network error
         */
        @Override
        public Bundle addAccount(AccountAuthenticatorResponse response, String accountType, String authTokenType, String[] requiredFeatures, Bundle options) throws NetworkErrorException {
            Bundle bundle = new Bundle();
            Intent intent = new Intent(appContext, MainActivity.class);
            intent.setAction(ACTION_LOGIN);
            intent.putExtra(AccountManager.KEY_ACCOUNT_TYPE, accountType);
            intent.putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, response);
            bundle.putParcelable(AccountManager.KEY_INTENT, intent);
            return bundle;
        }

        /**
         * Checks that the user knows the credentials of an account.
         *
         * @param response to send the result back to the AccountManager, will never be null
         * @param account  the account whose credentials are to be checked, will never be null
         * @param options  a Bundle of authenticator-specific options, may be null
         * @return a Bundle result or null if the result is to be returned via the response. The result
         * will contain either:
         * <ul>
         * <li> {@link AccountManager#KEY_INTENT}, or
         * <li> {@link AccountManager#KEY_BOOLEAN_RESULT}, true if the check succeeded, false otherwise
         * <li> {@link AccountManager#KEY_ERROR_CODE} and {@link AccountManager#KEY_ERROR_MESSAGE} to
         * indicate an error
         * </ul>
         * @throws NetworkErrorException if the authenticator could not honor the request due to a
         *                               network error
         */
        @Override
        public Bundle confirmCredentials(AccountAuthenticatorResponse response, Account account, Bundle options) throws NetworkErrorException {
            return null;
        }

        /**
         * Gets the authtoken for an account.
         *
         * @param response      to send the result back to the AccountManager, will never be null
         * @param account       the account whose credentials are to be retrieved, will never be null
         * @param authTokenType the type of auth token to retrieve, will never be null
         * @param options       a Bundle of authenticator-specific options, may be null
         * @return a Bundle result or null if the result is to be returned via the response. The result
         * will contain either:
         * <ul>
         * <li> {@link AccountManager#KEY_INTENT}, or
         * <li> {@link AccountManager#KEY_ACCOUNT_NAME}, {@link AccountManager#KEY_ACCOUNT_TYPE},
         * and {@link AccountManager#KEY_AUTHTOKEN}, or
         * <li> {@link AccountManager#KEY_ERROR_CODE} and {@link AccountManager#KEY_ERROR_MESSAGE} to
         * indicate an error
         * </ul>
         * @throws NetworkErrorException if the authenticator could not honor the request due to a
         *                               network error
         */
        @Override
        public Bundle getAuthToken(AccountAuthenticatorResponse response, Account account, String authTokenType, Bundle options) throws NetworkErrorException {
            return null;
        }

        /**
         * Ask the authenticator for a localized label for the given authTokenType.
         *
         * @param authTokenType the authTokenType whose label is to be returned, will never be null
         * @return the localized label of the auth token type, may be null if the type isn't known
         */
        @Override
        public String getAuthTokenLabel(String authTokenType) {
            return null;
        }

        /**
         * Update the locally stored credentials for an account.
         *
         * @param response      to send the result back to the AccountManager, will never be null
         * @param account       the account whose credentials are to be updated, will never be null
         * @param authTokenType the type of auth token to retrieve after updating the credentials,
         *                      may be null
         * @param options       a Bundle of authenticator-specific options, may be null
         * @return a Bundle result or null if the result is to be returned via the response. The result
         * will contain either:
         * <ul>
         * <li> {@link AccountManager#KEY_INTENT}, or
         * <li> {@link AccountManager#KEY_ACCOUNT_NAME} and {@link AccountManager#KEY_ACCOUNT_TYPE} of
         * the account that was added, or
         * <li> {@link AccountManager#KEY_ERROR_CODE} and {@link AccountManager#KEY_ERROR_MESSAGE} to
         * indicate an error
         * </ul>
         * @throws NetworkErrorException if the authenticator could not honor the request due to a
         *                               network error
         */
        @Override
        public Bundle updateCredentials(AccountAuthenticatorResponse response, Account account, String authTokenType, Bundle options) throws NetworkErrorException {
            return null;
        }

        /**
         * Checks if the account supports all the specified authenticator specific features.
         *
         * @param response to send the result back to the AccountManager, will never be null
         * @param account  the account to check, will never be null
         * @param features an array of features to check, will never be null
         * @return a Bundle result or null if the result is to be returned via the response. The result
         * will contain either:
         * <ul>
         * <li> {@link AccountManager#KEY_INTENT}, or
         * <li> {@link AccountManager#KEY_BOOLEAN_RESULT}, true if the account has all the features,
         * false otherwise
         * <li> {@link AccountManager#KEY_ERROR_CODE} and {@link AccountManager#KEY_ERROR_MESSAGE} to
         * indicate an error
         * </ul>
         * @throws NetworkErrorException if the authenticator could not honor the request due to a
         *                               network error
         */
        @Override
        public Bundle hasFeatures(AccountAuthenticatorResponse response, Account account, String[] features) throws NetworkErrorException {
            return null;
        }
    }
}
